﻿using NDO;

namespace NdoSqlCeTest
{
    [NDOPersistent]
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
