﻿using NDO;
using NDO.Linq;
using System;

namespace NdoSqlCeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var pm = new PersistenceManager();
            //var person = new Person { FirstName = "Chris", LastName = "Andritzky" };
            //pm.BuildDatabase();
            //pm.MakePersistent(person);
            //pm.Save();
            var person = pm.Objects<Person>().Where(p => p.FirstName.Like("%i%") && p.LastName.Like("A%")).FirstOrDefault();
            Console.WriteLine($"{person.FirstName} {person.LastName}");
        }
    }
}
